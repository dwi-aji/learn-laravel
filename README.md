<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Getting Started
- Clone this project
- Copy .env.example to .env
```
# Windows
copy .env.example .env
# Mac/Linux
cp .env.example .env
```
- Update .env value, match yours
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=learn_laravel
DB_USERNAME=root
DB_PASSWORD=password

# etc...
```
- Run composer command to install all required depedencies
```
composer install
```
- Generate Application Key
```
php artisan key:generate
```
- Run migration
```
php artisan migrate
```
- Run Laravel Server
```
php artisan serve
```

## To Do
Membuat fitur Create, Read, dan Edit untuk Product
1. t_product
- id*
- category_id**
- name

** berelasi dengan t_categories
=> Next membuat fitur delete untuk Category dan Product

## Artisan Command
1. Migration

Membuat file migration untuk men-create table di database
- Membuat table t_name
```
php artisan make:migration create_table_t_name --create=t_name
```
- Merubah susunan kolom pada table t_name
```
php artisan make:migration modify_table_t_name_column --table=t_name
```

2. Membuat Model

Model akan dibuat pada directory App/Models, berguna untuk menentukan koneksi dengan database dan bertindak untuk menggunakan Eloquent
```
php artisan make:model Models/ModelName
```

3. Membuat Controller

Controller berguna untuk menentukan model yang digunakan, pengolahan data akan dilakukan pada controller, sementara transaksi/perubahan data akan dilakukan oleh model yang dipanggil oleh Controller
```
php artisan make:controller NameOfController -m Models/ModelName
```

## REST
Metode pengiriman data yang Lazim digunakan pada Native PHP ada 2, yaitu: Post dan GET. Namun framework modern sudah menggunakan setidaknya 4 metode pengiriman data, yaitu:
- POST => Biasa digunakan untuk store data
- PUT => Biasa digunakan untuk update data
- GET => Biasa digunakan untuk menampilkan data
- DELETE => Biasa digunakan untuk menghapus data

Selain itu, format routing yang digunakan juga memiliki aturan tertentu, seperti
- GET /category => Menampilkan seluruh data category
- GET /category/{id} => Menampilkan data category dengan id tertentu
- GET /category/{id}/edit => Menampilkan form untuk merubah data category dengan id tertentu
- POST /category => Menyimpan data category
- PUT /category/{id} => Merubah data category dengan id tertentu
- DELETE /category/{id} => Menghapus data category

### Notes
- Penamaan file maupun class biasakan menggunakan "CamelCase" dengan awalan huruf besar
```
Do : CategoryModel | CategoryController
Dont : Categorymodel | Category_Model | categoryController
```
- Penamaan route gunakan dash (-) atau underscore (_)
```
Do : /tambah-data | /data-table | /get_year
Dont: /tambahData | /DataTable | /GetYear
```
- Penamaan variable biasakan menggunakan format "snake_case"
```
Do : $variable | $nama_variable
Dont : $var-iable | $namaVariable
```
- Penamaan function biasakan menggunakan format "camelCase" dengan awalan huruf kecil
```
Do : getYear() | thisIsFunction() | customFunctionForUploadData()
Dont: get_year() | GetYear()

*GetYear() akan ambigu antara nama function atau nama class
```

### References
- https://laravel.com/docs/6.x
- https://medium.com/@kiddy.xyz/restful-api-apaan-tuh-dbcfa434761e