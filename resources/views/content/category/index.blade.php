{{-- Menggunakan layout template pada directory /views/layout --}}
@extends('layout.template')

@section('css')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
@endsection

@section('content')
<div class="title m-b-md">
    Category
</div>

<a href="{{ route('category.create') }}" class="btn btn-primary mb-2">Buat Baru</a>

<table class="table table-bordered table-hover table-striped">
    <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @if (count($data) > 0)
        {{-- Jika data pada variable $data lebih dari 0 --}}
            @foreach ($data as $key => $item)
        <tr>
            <td>{{ $key }}</td>
            <td>{{ $item->name }}</td>
            <td>
                <a href="{{ route('category.edit', $item->id) }}" class="btn btn-warning">Edit</a>
            </td>
        </tr>
            @endforeach
        @else
        {{-- Jika data pada variable $data kosong --}}
        <tr>
            <td colspan="3">Tidak ada Data</td>
        </tr>
        @endif
    </tbody>
</table>
@endsection

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> 
@endsection