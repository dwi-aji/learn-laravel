<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Eloquent untuk Get semua Data Category
        $category = Category::all();
        return view('content.category.index', [
            // Digunakan untuk menyertakan variable $category ke view sebagai variable $data
            'data' => $category
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:191', 'unique:t_categories,name']
        ]);

        $data = Category::create([
            'name' => $request->name
        ]);
        return redirect()->route('category.index')->with([
            'status' => 'success',
            'message' => 'Berhasil menambahkan data'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Mencari Category dengan id $id, jika tidak ditemukan, maka akan mengembalikan halaman 404
        $category = Category::findOrFail($id);
        return view('content.category.edit', [
            'data' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:191', 'unique:t_categories,name,'.$id]
        ]);

        $data = Category::findOrFail($id);
        $data->name = $request->name;
        $data->save();
        
        return redirect()->route('category.index')->with([
            'status' => 'success',
            'message' => 'Berhasil merubah data'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
